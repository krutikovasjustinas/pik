package lt.kvk.i16.krutikovas;

import java.util.ArrayList;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import lt.kvk.i16.krutikovas.entity.Category;
import lt.kvk.i16.krutikovas.repository.CategoryRepository;
@RestController
@SpringBootApplication
public class PikNsApplication {
	@GetMapping("/getmessage")
	public String getMessage()
	{
		return "<img src = 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Flag_of_Lithuania.svg/640px-Flag_of_Lithuania.svg.png'>";
	}
	public static void main(String[] args) {
		SpringApplication.run(PikNsApplication.class, args);
	}
//	@Bean
//	CommandLineRunner commandLineRunner(CategoryRepository categoryRepository)
//	{
//		return args -> {
//			ArrayList<String> list = new ArrayList<String>();
//			list.add("Cannon");
//			list.add("Sony");
//			list.add("Niko");
//			list.add("Fujifilm");
//			list.add("Panasonic");
//			Category category = new Category("Fotoaparatai", list);
//			categoryRepository.save(category);
//		};
//	}

}
