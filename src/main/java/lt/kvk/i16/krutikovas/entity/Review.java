	package lt.kvk.i16.krutikovas.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;



@Entity
public class Review {
	@Id
	@SequenceGenerator(
			name = "review_sequence", 
			sequenceName = "review_sequence",
			allocationSize = 1)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "review_sequence")

	private int id;
	private int userId;
	private int recordId;
	private String body;
	private byte rating;
	private LocalDateTime dateTime;
	public Review(int userId, int recordId, String body, byte rating, LocalDateTime dateTime) {
		super();
		this.userId = userId;
		this.recordId = recordId;
		this.body = body;
		this.rating = rating;
		this.dateTime = dateTime;
	}
	public Review() {}
	
	public Review(int id, int userId, int recordId, String body, byte rating, LocalDateTime dateTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.recordId = recordId;
		this.body = body;
		this.rating = rating;
		this.dateTime = dateTime;
	}
	public int getId() {
		return id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public byte getRating() {
		return rating;
	}
	public void setRating(byte rating) {
		this.rating = rating;
	}
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	@Override
	public String toString() {
		return "Review [id=" + id + ", userId=" + userId + ", recordId=" + recordId + ", body=" + body + ", rating="
				+ rating + ", dateTime=" + dateTime + "]";
	}
	
	
	
	
}
