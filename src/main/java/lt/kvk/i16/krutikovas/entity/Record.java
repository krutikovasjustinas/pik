	package lt.kvk.i16.krutikovas.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Record implements Comparable<Record>{
	@Id
	@SequenceGenerator(
			name = "record_sequence", 
			sequenceName = "record_sequence",
			allocationSize = 1)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "record_sequence")

	private int id;
	private String title;
	private double price;
	
	private int categoryId;
	private boolean isLongTermLoan;
	@ElementCollection
	private Set<Integer> reviewsIdList;
//	@Embedded
//	private User author;
	public Record(String title, double price, int categoryId, boolean isLongTermLoan,
			Set<Integer> reviewsIdList/*, User author*/) {
		super();
		this.title = title;
		this.price = price;
		this.categoryId = categoryId;
		this.isLongTermLoan = isLongTermLoan;
		this.reviewsIdList = reviewsIdList;
//		this.author = author;
	}
	public Record() {}
	
	
	public Record(int id, String title, double price, int categoryId, boolean isLongTermLoan,
			Set<Integer> reviewsIdList/*, User author*/) {
		super();
		this.id = id;
		this.title = title;
		this.price = price;
		this.categoryId = categoryId;
		this.isLongTermLoan = isLongTermLoan;
		this.reviewsIdList = reviewsIdList;
//		this.author = author;
	}
	public int getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	@JsonProperty("isLongTermLoan")//spring boolean naming bug: solution: https://rakeshnarang.medium.com/what-the-hell-is-wrong-with-spring-boot-cant-convert-boolean-to-json-correctly-solution-aa5a418dd341
	public boolean isLongTermLoan() {
		return isLongTermLoan;
	}
	public void setLongTermLoan(boolean isLongTermLoan) {
		this.isLongTermLoan = isLongTermLoan;
	}
	public Set<Integer> getReviewsIdList() {
		return reviewsIdList;
	}
	public void setReviewsIdList(Set<Integer> reviewsIdList) {
		this.reviewsIdList = reviewsIdList;
	}
//	public User getAuthor() {
//		return author;
//	}
//	public void setAuthor(User author) {
//		this.author = author;
//	}
	@Override
	public String toString() {
		return "Record [id=" + id + ", title=" + title + ", price=" + price + ", categoryId=" + categoryId
				+ ", isLongTermLoan=" + isLongTermLoan + ", reviewsIdList=" + reviewsIdList/* + ", author=" + author
				*/+ "]";
	}
@Override
public int compareTo(Record o) {
	return this.id - o.getId();
}
	
	
	
}
