package lt.kvk.i16.krutikovas.entity;


public class RecordTitle {
	
	private int id;
	private String title;
	private String category;
	private double price;
	private double avgRating;
	
	public RecordTitle(String title, String category, double price, double avgRating) {
		super();
		this.title = title;
		this.category = category;
		this.price = price;
		this.avgRating = avgRating;
	}
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getAvgRating() {
		return avgRating;
	}
	public void setAvgRating(double avgRating) {
		this.avgRating = avgRating;
	}

}
