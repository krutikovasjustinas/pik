package lt.kvk.i16.krutikovas.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
@Entity
//@Embeddable
public class User {
	@Id
	@SequenceGenerator(
			name = "user_sequence", 
			sequenceName = "user_sequence",
			allocationSize = 1)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "user_sequence")

	private int id;
	private String username;
	private String password;
	private String email;
	@ElementCollection
	private Set<Integer> recordIdList;
	public User()
	{
	}
	public User(String username, String password, String email, Set<Integer> recordIdList) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.recordIdList = recordIdList;
	}
	
	public User(int id, String username, String password, String email, Set<Integer> recordIdList) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.recordIdList = recordIdList;
	}
	public int getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Set<Integer> getRecordIdList() {
		return recordIdList;
	}
	public void setRecordIdList(Set<Integer> recordIdList) {
//		if(gotDupeRecord(recordIdList))//apsauga nuo dupe record'u 
//			return;
		this.recordIdList = recordIdList;
	}
//	private boolean gotDupeRecord(List<Integer> recordIdList)
//	{
//		ArrayList<Integer> falseList = new ArrayList<Integer>();
//		for(int id: recordIdList)
//		{
//			falseList.add(id);
//			int count = 0;
//			for(int falseId: falseList) if(falseId==id) count++;
//			if(count>1) return true;
//		}
//		return false;
//	}
	
	
	
}
