package lt.kvk.i16.krutikovas.entity;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Category {
	@Id
	@SequenceGenerator(
			name = "category_sequence", 
			sequenceName = "category_sequence",
			allocationSize = 1)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "category_sequence"
			)
	private int id;
	private String name;
	@ElementCollection
	private List<String> brandNames;
	
	
	interface Observer {
        void update(String event);
    }
	
	class UpdateNameListener implements Observer{
		 private String name;
		 public UpdateNameListener(String name)
		 {
			 this.name = name;
		 }
		@Override
		public void update(String event) {
			System.out.println(event);
			
		}
	}
	@Transient
    private final List<Observer> observers = new ArrayList<>();
  

	private void notifyObservers(String event) {
        observers.forEach(observer -> observer.update(event));
    }
  
    public void addObserver(Observer observer) {
        observers.add(observer);
    }
	public Category()
	{
		
	}
	public Category(String name, List<String> brandNames) {
		super();
		this.name = name;
		this.brandNames = brandNames;
	}
	

	public Category(int id, String name, List<String> brandNames) {
		super();
		this.id = id;
		this.name = name;
		this.brandNames = brandNames;
	}
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		notifyObservers("Name changed event");
	}

	public List<String> getBrandNames() {
		return brandNames;
	}

	public void setBrandNames(List<String> brandNames) {
		this.brandNames = brandNames;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + ", brandNames=" + brandNames + "]";
	}
	//pasiziureti builder DP Telusko
//	public Category builderSetName(String name) 
//	{
//		this.name = name;
//		return this;
//	}
//	
//	public Category builderSetBrandNames(List<String> brandNames)
//	{
//		this.brandNames = brandNames;
//		return this;
//	}
}
