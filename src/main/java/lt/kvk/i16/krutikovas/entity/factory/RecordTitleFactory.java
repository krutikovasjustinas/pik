package lt.kvk.i16.krutikovas.entity.factory;

import lt.kvk.i16.krutikovas.computation.RatingCalculator;
import lt.kvk.i16.krutikovas.entity.Category;
import lt.kvk.i16.krutikovas.entity.Record;
import lt.kvk.i16.krutikovas.entity.RecordTitle;

public class RecordTitleFactory {
	public RecordTitle create(Record rec, Category cat)
	{
		return new RecordTitle(rec.getTitle(), cat.getName(), rec.getPrice(), RatingCalculator.calculate(rec.getReviewsIdList()));
	}
}
