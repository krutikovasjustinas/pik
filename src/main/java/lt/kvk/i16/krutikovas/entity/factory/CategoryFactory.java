package lt.kvk.i16.krutikovas.entity.factory;

import java.util.List;

import lt.kvk.i16.krutikovas.entity.Category;

public class CategoryFactory {
	public static Category create(String name, List<String> brands)
	{
		return new Category(name, brands);
	}
}
