package lt.kvk.i16.krutikovas.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i16.krutikovas.entity.User;
import lt.kvk.i16.krutikovas.repository.UserRepository;

@RestController
@RequestMapping("/api/users")
@CrossOrigin
public class UserController {
	@Autowired
	private UserRepository userRepo;
	
	@GetMapping("")
	public ResponseEntity<List<User>> getAllUsers()
	{
		return ResponseEntity.ok()
//				.header("Access-Control-Allow-Origin","*")
				.body(userRepo.findAll());
	}
	
	@PostMapping("/save")
	public ResponseEntity<User> addUser(@Validated @RequestBody User user)
	{
		userRepo.save(user);
		return ResponseEntity.ok()
			.build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> getUser(@PathVariable(name = "id") int id) throws Exception
	{
		User user = null;
		try{
			user = userRepo.findById(id).get();
		}
		catch(NoSuchElementException e)
		{
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(user);	
	}
	@PutMapping("/update/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(name = "id") int id, @RequestBody User newUser)
	{
		User user = userRepo.findById(id).get();
		user.setUsername(newUser.getUsername());
		user.setPassword(newUser.getPassword());
		user.setRecordIdList(newUser.getRecordIdList());
		user.setEmail(newUser.getEmail());
		userRepo.save(user);
		return null;
	}
	
	@GetMapping("/delete/{id}")
	public ResponseEntity<User> deleteUser(@PathVariable(name = "id") int id)
	{
		User user = null;
		try{
			user = userRepo.findById(id).get();
			userRepo.deleteById(id);
		}
		catch(NoSuchElementException e)
		{
			e.printStackTrace();
			return ResponseEntity.ok().body(user);
		}
		return ResponseEntity.ok().build();
	}
}
