package lt.kvk.i16.krutikovas.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i16.krutikovas.adapter.RecordTitleAdapter;
import lt.kvk.i16.krutikovas.entity.RecordTitle;
import lt.kvk.i16.krutikovas.entity.Record;
import lt.kvk.i16.krutikovas.repository.Repository;

@RestController
@RequestMapping("/api/recordtitle")
@CrossOrigin
public class RecordTitleController {

	@GetMapping("/{id}")
	public ResponseEntity<RecordTitle> getRecordTitle(@PathVariable(name = "id") int id) throws Exception {
		Record r = Repository.recordRepo.findById(id).get();
		return ResponseEntity.ok().body(
				new RecordTitleAdapter(r, Repository.categoryRepo.findById(r.getCategoryId()).get()).getRecordTitle());
	}

	@GetMapping("")
	public ResponseEntity<List<RecordTitle>> getRecordTitleList() {
		List<Record> recs = new ArrayList<>();
		List<RecordTitle> recsTitles = new ArrayList<>();
		for (Record r : recs)
			recsTitles.add(new RecordTitleAdapter(r, Repository.categoryRepo.findById(r.getCategoryId()).get())
					.getRecordTitle());
		return ResponseEntity.ok().body(recsTitles);
	}
}
