package lt.kvk.i16.krutikovas.controller;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i16.krutikovas.entity.Record;
import lt.kvk.i16.krutikovas.entity.User;
import lt.kvk.i16.krutikovas.repository.RecordRepository;
import lt.kvk.i16.krutikovas.repository.Repository;

@RestController
@RequestMapping("/api/records")
@CrossOrigin
public class RecordController {
	@Autowired
	private RecordRepository recordRepo;

	@GetMapping("")
	public ResponseEntity<List<Record>> getAllRecords() {
		return ResponseEntity.ok()
				.body(recordRepo.findAll());
	}
	@GetMapping("sorted_records_by_id")
	public ResponseEntity<List<Record>> getSortedByIdRecords() {
		List<Record> records = recordRepo.findAll();
		Collections.sort(records);
		return ResponseEntity.ok()
				.body(records);
	}

	@PostMapping("/save")
	public ResponseEntity<Record> addRecord(@Validated @RequestBody Record rec) {
		System.out.println(rec.isLongTermLoan());
		recordRepo.save(rec);
//		User user = rec.getAuthor();
//		List<Integer> li = user.getRecordIdList();
//		li.add(rec.getId());
//		user.setRecordIdList(li);
//		Repository.userRepo.save(user);
		return ResponseEntity.ok().body(rec);

	}

	@GetMapping("/{id}")
	public ResponseEntity<Record> getRecord(@PathVariable(name = "id") int id) throws Exception {
		Record rec = null;
		try {
			rec = recordRepo.findById(id).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok().body(rec);
	}
	
	@GetMapping("/long_term_records")// elementu atranka kolekcijoje
	public ResponseEntity<List<Record>> getLongTermRecords(@PathVariable(name = "id") int id) throws Exception {
		List<Record> records = recordRepo.findAll();
		for(Record rec: records)
			if(!rec.isLongTermLoan())
				records.remove(rec);
		return ResponseEntity.ok()
				.body(records);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Record> updateRecord(@PathVariable(name = "id") int id, @RequestBody Record newRecord) {
		Record rec = recordRepo.findById(id).get();
		rec.setTitle(newRecord.getTitle());
		rec.setPrice(newRecord.getPrice());
		rec.setCategoryId(newRecord.getCategoryId());
		rec.setLongTermLoan(newRecord.isLongTermLoan());
		rec.setReviewsIdList(newRecord.getReviewsIdList());
		recordRepo.save(rec);
		return ResponseEntity.ok().body(rec);
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<Record> deleteRecord(@PathVariable(name = "id") int id) {
		Record rec = null;
		try {
			rec = recordRepo.findById(id).get();
//			User user = rec.getAuthor();
//			List<Integer> li = user.getRecordIdList();
//			for (int i = 0; i < li.size(); i++) {
//				if (li.get(i) == id) {
//					li.remove(i);
//					break;
//				}
//			}
//			user.setRecordIdList(li);
//			Repository.userRepo.save(user);
			recordRepo.deleteById(id);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return ResponseEntity.ok().body(rec);
		}
		return ResponseEntity.ok().body(rec);
	}
}
