package lt.kvk.i16.krutikovas.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i16.krutikovas.entity.Category;
import lt.kvk.i16.krutikovas.repository.CategoryRepository;

@RestController
@RequestMapping("/api/category")
@CrossOrigin
public class CategoryController {
	@Autowired
	private CategoryRepository categoryRepo;
	
	@GetMapping("")
	public ResponseEntity<List<Category>> getAllCategories()
	{
		return ResponseEntity.ok()
				.body(categoryRepo.findAll());
	}
	
	@GetMapping("startfromb")
	public ResponseEntity<List<Category>> getAlleveryCategoryStartingFromC()
	{
		List<Category> categoryList = categoryRepo.findAll();
		List<Category> filteredList = new ArrayList<Category>();
		for(Category cat: categoryList)
			filteredList.add(cat);
		return ResponseEntity.ok()
				.body(filteredList);
		
	}
	
	@PostMapping("/save")
	public ResponseEntity<Category> addCategory(@Validated @RequestBody Category cat)
	{
		categoryRepo.save(cat);
		return ResponseEntity.ok()
			.build();	
	}
	@GetMapping("/{id}")
	public ResponseEntity<Category> getCategory(@PathVariable(name = "id") int id) throws Exception
	{
		Category cat = null;
		try{
			cat = categoryRepo.findById(id).get();
		}
		catch(NoSuchElementException e)
		{
			e.printStackTrace();
		}
		
		return ResponseEntity.ok().body(cat);	
	}
	@PutMapping("/update/{id}")
	public ResponseEntity<Category> updateCategory(@PathVariable(name = "id") int id, @RequestBody Category newCategory)
	{
		Category cat = categoryRepo.findById(id).get();
		cat.setName(newCategory.getName());
		cat.setBrandNames(newCategory.getBrandNames());
		categoryRepo.save(cat);
//		return ResponseEntity.ok().body(cat);
		return null;
	}
	
	@GetMapping("/delete/{id}")
	public ResponseEntity<Category> deleteCategory(@PathVariable(name = "id") int id)
	{
		Category cat = null;
		try{
			cat = categoryRepo.findById(id).get();
		}
		catch(NoSuchElementException e)
		{
			e.printStackTrace();
			return ResponseEntity.ok().body(cat);
		}
		
		categoryRepo.deleteById(id);
//		return ResponseEntity.ok().body(cat);
		return null;
	}
}