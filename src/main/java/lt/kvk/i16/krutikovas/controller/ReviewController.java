package lt.kvk.i16.krutikovas.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lt.kvk.i16.krutikovas.entity.Review;
import lt.kvk.i16.krutikovas.repository.ReviewRepository;

@RestController
@RequestMapping("/api/reviews")
@CrossOrigin
public class ReviewController {
	@Autowired
	private ReviewRepository reviewRepo;

	@GetMapping("")
	public ResponseEntity<List<Review>> getAllRecords() {
		return ResponseEntity.ok()
//				.header("Access-Control-Allow-Origin","*")
				.body(reviewRepo.findAll());
	}

	@PostMapping("/save")
	public ResponseEntity<Review> addReview(@Validated @RequestBody Review rev) {
		reviewRepo.save(rev);
		return ResponseEntity.ok().build();

	}

	@GetMapping("/{id}")
	public ResponseEntity<Review> getRecord(@PathVariable(name = "id") int id) throws Exception {
		Review rev = null;
		try {
			rev = reviewRepo.findById(id).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok().body(rev);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Review> updateRecord(@PathVariable(name = "id") int id, @RequestBody Review newReview) {
		Review rev = reviewRepo.findById(id).get();
		rev.setUserId(newReview.getUserId());
		rev.setRecordId(newReview.getRecordId());
		rev.setBody(newReview.getBody());
		rev.setRating(newReview.getRating());
		rev.setDateTime(newReview.getDateTime());

		return ResponseEntity.ok().body(rev);
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<Review> deleteRecord(@PathVariable(name = "id") int id) {
		Review rev = null;
		try {
			rev = reviewRepo.findById(id).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return ResponseEntity.ok().body(rev);
		}

		reviewRepo.deleteById(id);
		return ResponseEntity.ok().body(rev);
	}
}
