package lt.kvk.i16.krutikovas.computation;


import java.util.Set;

import lt.kvk.i16.krutikovas.repository.Repository;

public class RatingCalculator {
	
	public static double calculate(Set<Integer> ratingList)
	{
		double sumOfRating=0;
		for(int rating: ratingList)
			sumOfRating+=Repository.reviewRepo
				.findById(rating).get().getRating();
		sumOfRating /= ratingList.size();
		return Double.parseDouble(String.format("%,.2f", sumOfRating));
	}
	
	
}
