package lt.kvk.i16.krutikovas.adapter;

import lt.kvk.i16.krutikovas.entity.Category;
import lt.kvk.i16.krutikovas.entity.Record;
import lt.kvk.i16.krutikovas.entity.RecordTitle;
import lt.kvk.i16.krutikovas.entity.factory.RecordTitleFactory;

public class RecordTitleAdapter implements RecordTitleAdapterInterface{
	private Record record;
	private Category category;
	
	public RecordTitleAdapter(Record r, Category c)
	{
		this.record = r;
		this.category = c;
	}
	
	@Override
	public RecordTitle getRecordTitle() {
		return new RecordTitleFactory().create(record, category);
	}
	
}
