package lt.kvk.i16.krutikovas.adapter;

import lt.kvk.i16.krutikovas.entity.Category;
import lt.kvk.i16.krutikovas.entity.Record;
import lt.kvk.i16.krutikovas.entity.RecordTitle;

public interface RecordTitleAdapterInterface {
//	public Record record = new Record();
//	public Category category = new Category();
	
	RecordTitle getRecordTitle();
}
