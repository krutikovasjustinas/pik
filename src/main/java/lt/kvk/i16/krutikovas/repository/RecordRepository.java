package lt.kvk.i16.krutikovas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import lt.kvk.i16.krutikovas.entity.Record;

import org.springframework.stereotype.Repository;
@Repository
public interface RecordRepository extends JpaRepository<Record, Integer> {

}
