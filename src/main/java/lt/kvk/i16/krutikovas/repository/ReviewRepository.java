package lt.kvk.i16.krutikovas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lt.kvk.i16.krutikovas.entity.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer>{

}
