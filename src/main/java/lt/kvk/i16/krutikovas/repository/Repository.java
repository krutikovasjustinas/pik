package lt.kvk.i16.krutikovas.repository;

import org.springframework.beans.factory.annotation.Autowired;

public class Repository {
	@Autowired
	public static CategoryRepository categoryRepo;
	@Autowired
	public static RecordRepository recordRepo;
	@Autowired
	public static ReviewRepository reviewRepo;
	@Autowired
	public static UserRepository userRepo;
	
}
