package lt.kvk.i16.krutikovas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lt.kvk.i16.krutikovas.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
