export class Category {
    public id:number;
    public name:string;
    public brandNames: string[];

    constructor(id: number, name: string, brandNames: string[]) {
        this.id = id;
        this.name = name;
        this.brandNames = brandNames;
      }
}

