import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { User } from './user.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _getAllUrl: string = "http://localhost:8080/api/users/";
  private _createUrl: string = "http://localhost:8080/api/users/save";
  private _deleteUrl: string = "http://localhost:8080/api/users/delete/";
  private _updateUrl: string = "http://localhost:8080/api/users/update/";
  private httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: 'my-auth-token'
    })}

  constructor(private http: HttpClient) { }

  addUser(category: User): Observable<any>{
    return  this.http.post<User>(this._createUrl, category, this.httpOptions);
  }
   getUsers(): Observable<User[]>
   {
      return this.http.get<User[]>(this._getAllUrl);
   }
   
   updateUser(category: User): Observable<any>
   {
      return this.http.put<User>(this._updateUrl + category.id, category);
   }

   deleteUser(id: number): Observable<User>
   {
      return this.http.get<User>(this._deleteUrl+id);
   }
}
