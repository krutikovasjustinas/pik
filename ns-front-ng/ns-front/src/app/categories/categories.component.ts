import { Component, OnInit } from '@angular/core';
import { Category } from '../category.model';
import { CategoryService } from '../category.service';



@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass']
})
export class CategoriesComponent implements OnInit {
    public categories: Category[] = [];
    public categoryName: string = '';
    public brandNames: string = '';
    public updateName: string = '';
    public updateBrandNames: string = '';
    public editingNow: number = 0;
    constructor(private _categoryService: CategoryService) { }

    ngOnInit(): void {
      this.refreshCategories();
    }
    refreshCategories() {
      this._categoryService.getCategories()
      .subscribe(data => this.categories = data);
      
    }
    addCategory(): void{

      let category = new Category(0, this.categoryName, this.parseCategoryArray(this.brandNames));
      this._categoryService.addCategory(category)
      .subscribe(data => {
        console.log(data)
        this.ngOnInit();
      }, err => {
        console.log(err);
        this.ngOnInit();});

    }
    parseCategoryArray(brandsNames: string): string[]
    {
      return brandsNames.split(", ");
    }
    deleteCategory(id: number): void{
      this._categoryService.deleteCategory(id)
      .subscribe(data => {
        console.log(data);
      }, err=>{console.log(err);
      this.ngOnInit();});
      for(var i=0;i<this.categories.length;i++)
      {
        if(this.categories[i].id==id)
        {
          this.categories.splice(i, 1);
          break;
        }
      }
    }
    
    initializeEdit(id:number): void{
      this.editingNow = id;
    }
    updateCategory(): void{
      if(this.editingNow==0)
        return;
      var newCategory = new Category(this.editingNow, this.updateName, this.parseCategoryArray(this.updateBrandNames));
      this._categoryService.updateCategory(newCategory)
      .subscribe(data => console.log(data));
      for(var i = 0; i < this.categories.length; i++)
      {
        if(this.categories[i].id==this.editingNow)
        {
          this.categories[i]=newCategory;
          break;
        }
      }
      this.editingNow = 0;
    }
    
    editThis(id: number): boolean{
      return id==this.editingNow;
    }
  }

