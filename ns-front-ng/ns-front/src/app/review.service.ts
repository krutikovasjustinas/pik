import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Review } from './review.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private _getAllUrl: string = "http://localhost:8080/api/reviews/";
private _createUrl: string = "http://localhost:8080/api/reviews/save";
private _deleteUrl: string = "http://localhost:8080/api/reviews/delete/";
private _updateUrl: string = "http://localhost:8080/api/reviews/update/";
private httpOptions: any = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

  constructor(private http: HttpClient) {}
  addReview(category: Review): Observable<any>{
    return  this.http.post<Review>(this._createUrl, category, this.httpOptions);
  }
   getReviews(): Observable<Review[]>
   {
      return this.http.get<Review[]>(this._getAllUrl);
   }
   
   updateReview(review: Review): Observable<any>
   {
      return this.http.put<Review>(this._updateUrl + review.id, review);
   }

   deleteReview(id: number): Observable<Review>
   {
      return this.http.get<Review>(this._deleteUrl+id);
   }

   private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}
