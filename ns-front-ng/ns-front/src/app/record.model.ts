export class Record {
    public id: number;
    public title: string;
    public price: number;
    public categoryId: number;
    public isLongTermLoan: boolean;
    public reviewsIdList: number[];
    constructor(id:number, title:string, price:number, categoryId:number, isLongTermLoan:boolean, reviewsIdList:number[])
    {
        this.id = id;
        this.title = title;
        this.price = price;
        this.categoryId = categoryId;
        this.isLongTermLoan = isLongTermLoan;
        this.reviewsIdList = reviewsIdList;
    }
}
