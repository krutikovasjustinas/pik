export class Review {
    public id: number;
    public userId: number;
    public recordId: number;
    public body: string;
    public rating: number;
    public dateTime: Date;

    constructor(id: number, userId: number, recordId: number, body: string, rating: number, dateTime: Date)
    {
        this.id = id;
        this.userId = userId;
        this.recordId = recordId;
        this.body = body;
        this.rating = rating;
        this.dateTime = dateTime;
    }
}
