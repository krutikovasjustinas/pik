export class User {
    public id:number;
    public username:string;
    public password: string;
    public email: string;
    public recordIdList: number[];

    constructor(id: number, username: string, password: string, email: string, recordIdList: number[]) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.recordIdList = recordIdList;
      }
}

