import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Record } from './record.model';

@Injectable({
  providedIn: 'root'
})
export class RecordService {
  private _getAllUrl: string = "http://localhost:8080/api/records/";
  private _createUrl: string = "http://localhost:8080/api/records/save/";
  private _deleteUrl: string = "http://localhost:8080/api/records/delete/";
  private _updateUrl: string = "http://localhost:8080/api/records/update/";
  private httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) { }

  addRecord(record: Record): Observable<any>{
    return  this.http.post<Record>(this._createUrl, record, this.httpOptions);
  }
   getRecords(): Observable<Record[]>
   {
      return this.http.get<Record[]>(this._getAllUrl);
   }
   
   updateRecord(record: Record): Observable<any>
   {
      return this.http.put<Record>(this._updateUrl + record.id, record);
   }

   deleteRecord(id: number): Observable<Record>
   {
      return this.http.get<Record>(this._deleteUrl+id);
   }

   private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}
