import {  Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { Record } from '../record.model';
import { UserService } from '../user.service';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit {
  public users: User[] = [];
  public records: Record[] = [];
  public username: string = '';
  public password: string = '';
  public email: string = '';
  public recordIdList: number[] = [];
  public recordId: number = 0;
  public freeRecords: Record[] = [];
  public addRecordId: number = 0;




  public editingNow: number = 0;
  constructor(private _userService: UserService, private _recordService: RecordService) { }


  ngOnInit(): void {
    
    this._recordService.getRecords()
    .subscribe(data => this.records = data);
    this.refreshUsers();

  
      
  }
  findInArrayIndex(record: number ,arr: Record[]): number
  {
    for(var i = 0; i < arr.length; i++)
      if(arr[i].id == record)
        return i;
    return 0;
  }
  refreshUsers(): void {
    this._userService.getUsers()
    .subscribe(data => {this.users = data});
  }
  initializeFreeUserArray(data: User[])
  {
    this.freeRecords = this.records;
    for(var user of data)// user
    {
      console.log(user);
      for(var rec of user.recordIdList)// recordIdList
      {
        console.log(rec)
        for(var recd of this.records)// records
        {
          console.log("recd: " + recd.id + "rec: " + rec)
          console.log(rec == recd.id);
          if(rec == recd.id)
          {
            this.freeRecords.splice(this.findInArrayIndex(rec, this.freeRecords), 1);
          }
        }
      }
    }
    console.log("after:");
    console.log(this.records);
    console.log(this.freeRecords);
  }
  
  addUser(): void {
    if(this.recordId!=0)
      this.recordIdList.push(this.recordId);
    var user = new User(0, this.username, this.password, this.email, this.recordIdList);
    this._userService.addUser(user)
    .subscribe(data => {
      console.log(data)
      this.ngOnInit();
    }, err => {
      console.log(err);
      this.ngOnInit();});
      this.refreshUsers();
  }
  deleteUser(id: number): void{
    this._userService.deleteUser(id)
    .subscribe(data => {
      console.log(data);
    }, err=>{console.log("tai ka yra");
    this.ngOnInit();});
    for(var i = 0; i < this.users.length; i++)
    {
      if(this.users[i].id==id)
      {
        this.users.splice(i, 1);
        break;
      }
    }
  }

  findById(id: number): User{
    for(var user of this.users)
      if(user.id == id)
        return user;
    return this.users[0];
  }
  initializeEdit(id:number): void{
    this.editingNow = id;
    var user = this.findById(id);
    this.username = user.username;
    this.password = user.password;
    this.email = user.email;
  }
  cancelEdit()
  {
    this.editingNow = 0;
    this.username = "";
    this.password = "";
    this.email = "";
  }

  updateUser(): void{
    if(this.editingNow==0)
      return;
    if(this.addRecordId!=0)
      this.recordIdList.push(this.addRecordId);
    var newUser: User = new User(this.editingNow, this.username, this.password, this.email,  this.recordIdList);
    this._userService.updateUser(newUser)
    .subscribe(data => {newUser = data});

    for(var i = 0; i< this.users.length; i++)
    {
      if(this.users[i].id==this.editingNow)
      {
        this.users[i]=newUser;
        break;
      }
    }
    this.editingNow = 0;
  }
  editThis(id: number): boolean{
    return id==this.editingNow;
  }

}
