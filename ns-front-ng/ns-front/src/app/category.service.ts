import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { Category } from './category.model';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

private _getAllUrl: string = "http://localhost:8080/api/category/";
private _createUrl: string = "http://localhost:8080/api/category/save/";
private _deleteUrl: string = "http://localhost:8080/api/category/delete/";
private _updateUrl: string = "http://localhost:8080/api/category/update/";
private httpOptions: any = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

  constructor(private http: HttpClient) {}
  addCategory(category: Category): Observable<any>{
    return  this.http.post<Category>(this._createUrl, category, this.httpOptions);
  }
   getCategories(): Observable<Category[]>
   {
      return this.http.get<Category[]>(this._getAllUrl);
   }
   
   updateCategory(category: Category): Observable<any>
   {
      return this.http.put<Category>(this._updateUrl + category.id, category);
   }

   deleteCategory(id: number): Observable<Category>
   {
      return this.http.get<Category>(this._deleteUrl+id);
   }

   private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}
