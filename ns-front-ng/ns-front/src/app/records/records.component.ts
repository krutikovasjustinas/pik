import { Component, OnInit } from '@angular/core';
import { Category } from '../category.model';
import { CategoryService } from '../category.service';
import { Record } from '../record.model';
import { RecordService } from '../record.service';
import { Review } from '../review.model';
import { ReviewService } from '../review.service';

@Component({
  selector: 'app-records',
  templateUrl: './records.component.html',
  styleUrls: ['./records.component.sass']
})
export class RecordsComponent implements OnInit {
    public records: Record[] = [];
    public categories: Category[] = [];
    public reviews: Review[] = [];

    public title: string = '';
    public price: number = 0;
    public categoryId: number = 0;
    public isLongTermLoan: boolean = false;
    public reviewsIdList: number[] = [];

    public updateTitle: string = '';
    public updatePrice: number = 0;
    public updateCategoryId: string = "";
    public updateIsLongTermLoan: string = "";
    public updateReviewsIdList: number[] = [];

    public editingNow: number = 0;

  constructor(private _recordService: RecordService, private _categoryService: CategoryService, private _reviewService: ReviewService) { }

  ngOnInit(): void {
    this._reviewService.getReviews().subscribe(data=>this.reviews=data);
    this._categoryService.getCategories().subscribe(data=>this.categories=data);
    this.refreshRecords();
  }

  refreshRecords()
  {
    this._recordService.getRecords()
    .subscribe(data=> this.records = data);
  }
  addRecord()
  {
    var rec = new Record(0, this.title, this.price, this.categoryId, this.isLongTermLoan, []);
    this._recordService.addRecord(rec)
    .subscribe(data=>{this.records.push(data); console.log(this.records)}, err=>console.log(err));
    // this.ngOnInit();
  }
  private getCategoryId(name: string): number
  {
    for(var cat of this.categories)
      if(cat.name==name)
        return cat.id;
    return 0;
  }
  deleteRecord(id: number)
  {
    this._recordService.deleteRecord(id).subscribe(data=>{});
    this.removeFromRecordArray(id);
  }
  initializeEdit(id:number)
  {
    this.editingNow=id;
    var rec: Record = this.records[0];
    for(var recd of this.records)
      if(recd.id==id)
        rec=recd;
    this.title=rec.title;
    this.price=rec.price;
    this.categoryId=rec.categoryId;
    this.isLongTermLoan=rec.isLongTermLoan;
    console.log(this.isLongTermLoan);
  }
  removeFromRecordArray(id:number)
  {
    for(var i = 0; i < this.records.length; i++){
      if(this.records[i].id==id)
      {
        this.records.splice(i, 1);
        break;
      }
    }
  }
  updateRecordArray(rec: Record)
  {
    for(var i = 0; i < this.records.length; i++)
    {
      if(this.records[i].id == rec.id)
      {
        this.records[i]=rec;
        break;
      }
    }
  }
  editThis(id: number): boolean
  {
    return id == this.editingNow;
  }
  updateRecord()
  {
    if(this.editingNow==0)
        return;
    var formerRecord: Record = this.records[0];
    for(var rec of this.records)
    {
      if(rec.id==this.editingNow)
      {
        formerRecord = rec;
        break;
      }
    }  
    var record: Record = new Record(this.editingNow, this.title, this.price, this.categoryId, this.isLongTermLoan, formerRecord.reviewsIdList);
    this._recordService.updateRecord(record)
    .subscribe(data => this.updateRecordArray(data));
    this.editingNow = 0;
  }
  cancelEdit(){
    this.editingNow = 0;
    this.title = "";
    this.categoryId = 0;
    this.isLongTermLoan = false;
    this.price = 0;
    this.reviewsIdList = [];
  }
}
