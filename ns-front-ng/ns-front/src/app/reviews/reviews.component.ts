import { Component, OnInit } from '@angular/core';
import { Review } from '../review.model';
import { ReviewService } from '../review.service';
import {FormControl} from '@angular/forms';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { Record } from '../record.model';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.sass']
})
export class ReviewsComponent implements OnInit {

    public users: User[] = [];
    public records: Record[] = [];
    public reviews: Review[] = [];
    public categoryName: string = '';
    public userId: number = 0;
    public recordId: number = 0;
    public body: string = '';
    public rating: number = 0;
    public editingNow: number = 0;
   constructor(private _reviewService: ReviewService, private _userService: UserService, private _recordService: RecordService) { }

  ngOnInit(): void {
    this._recordService.getRecords()
    .subscribe(data => this.records = data);
    this._userService.getUsers()
    .subscribe(data => this.users = data);
    this.refreshReviews();
  }
  refreshReviews() {
    this._reviewService.getReviews()
    .subscribe(data => this.reviews = data);
    
  }
  addReview()
  {
    let review = new Review(0, this.userId, this.recordId, this.body, this.rating, new Date());
      this._reviewService.addReview(review)
      .subscribe(data => {
        console.log(data)
        this.ngOnInit();
      }, err => {
        console.log(err);
        this.ngOnInit();});

  }
  deleteReview(id: number)
  {
    this._reviewService.deleteReview(id).subscribe(data=>{console.log(data); this.ngOnInit()}, err=>{console.log(err); this.ngOnInit();});
  }

  updateReview()
  {
    if(this.editingNow == 0)
      return;
    var id = this.editingNow;
    var rev = this.findById(id);
    var newRev = new Review(id, this.userId, this.recordId, this.body, this.rating, rev.dateTime);
    this._reviewService.updateReview(newRev)
    .subscribe(data => newRev = data);
    for(var i = 0; i < this.reviews.length; i++)
    {
      if(this.reviews[i].id == newRev.id)
      {
        this.reviews[i] = newRev;
        break;
      }
    }
    this.cancelEdit;
  }

  findById(id: number): Review
  {
    for(var rev of this.reviews)
    {
      if(rev.id == id)
        return rev;
    }
    return this.reviews[0];
  }
  initializeEdit(id: number): void{
    this.editingNow = id;
    var rev = this.findById(id);
    this.userId = rev.userId;
    this.recordId = rev.recordId;
    this.body = rev.body;
    this.rating = rev.rating;
  }
  cancelEdit(){
    this.editingNow = 0;
    this.userId = 0;
    this.recordId = 0;
    this.body = "";
    this.rating = 0;
  }
  editThis(id: number)
  {
    return this.editingNow == id;
  }
}
